# SearchThapp

In this repository, we will create a search app for items at MeLi with MVVM, Security Concepts and CI.

This app will NOT have in account some nice-to-have visual elements like advanced animations, motion layout, or use of compose. The UI will be neat as possible with a trade-off of nice UI/ time. 

We will focus instead on architecture, CI, security, and maybe on how to build a multimodule app for AppGallery (HMS) and for GooglePlay (GMS) using Gradle.

Thank you for read this post, this app was made with love and coffe from Colombia 🇨🇴☕️💛

![](readmeResources/ui.gif)
ps: gif is in a low framerate hehe, the app is faster

# Summary

- MVVM Architecture + Room + Retrofit + LiveData. No Databinding. No Compose.

- Nowadays, you have to consider AppGallery too i.e: For analytics, you should use HuaweiAnalytics for AppGallery and FirebaseAnalytics for GooglePlay, `gradle assemble` builds an APK (`assemble and debug`) for both stores, this is a clean way to ship FirebaseAnalytics and HuaweiAnalytics isolated from each other. _HuaweiAnalytics and FirebaseAnalytics are mocked, just for demonstrating how to build that kind of scheme_

- The app is backed up by 35 tests, you can execute the coverage using `gradle jacocoTestReport` or in short `gradle jTR` the tests on the app will start to run and you will see the report on _app/build/reports/jacoco/jacocoTestReport/html/index.html_ or right on the gitlab Merge Request, our script will convert the coverage Jacoco Format to a Gitlab Format. At the moment the app has 88% of coverage if you want to make a MR please ensure that your code gets at least 80% of coverage.

- Minify for reverse engineering, Pinning for MITM attacks.

- Gitlab Runner on a GoogleCloud VM with docker, ssh tunnel to connect runner with a physical phone, please feel free to visit [my medium post](https://medium.com/nerd-for-tech/android-continuous-integration-and-coverage-visualization-on-gitlab-4a15d58f624c) to see how you can do it and [this if you want to setup an isolated Gitlab Repository Server with that setup](https://medium.com/nerd-for-tech/deploying-a-gitlab-repository-and-runner-server-on-google-cloud-57b9d490c975)

# UI

We will write a very simply UI composed by a Host Activity, Fragment Container and a Detailed Activity.

This section will refer to the code under app/src/main/java/com/amaca/searchthapp/views/ and the paths of this section will be relative to it.


### Expected behaivor

In the beginning, the user can see the recent search item (RecentSearchFragment) and the search bar. At this point user can:
- Click on any searched item so the DetailedViewActivity will be open and show the details of the item
- Search for a new Item with the search bar, when the user tap on search, the desired items will appear on the CurrentSearchFragment and the user can click on any item to see more details.

Considerations:
- adapters/SearchAdapter.kt is a child class of ListAdapter: Avoids to re-draw all the views when the dataset changes, it's useful when we have a lot of data. Checks the minimum changes to get the newdataset from the olddataset using Myers' difference algorithm, see how at https://blog.robertelder.org/diff-algorithm/
- SearchAdapter is used on both, RecentSearchFragment and CurrentSearchFragment due to they maintain the same data displayed in different ways. Aside from performance. Adapters are a very useful component and can be reused when you want to display the same data in diverse ways.
 
- The main idea is UI is decoupled from the data, one powerful reason is to make unit/instrumentation testing simpler, you can see the way that we test the UI on the CI-UI section. 

Note: The SearchBar animation is the only animation on all the app (the movement is calculated using a simple linear equation, but it can be a quadratic equation to make the thing smoother and follow the Guidelines from Material, the code is under helper/SearchBarAnimationHelper.kt) 

# Architecture.

MVVM is a well-known architecture that allows us as developers build a scalable apps.
- The app architecture decouples the data layer from the UI layer allowing:
- Test each element in an isolated environment, 
- Reusability of the components on other projects.
- Interchangeability between app's dependencies i.e: you can easily change retrofit network implementation from another one without modifying any part of the code but the network one.

Many many more, you can dig deep on MVVM: [official guide](https://developer.android.com/jetpack/guide) and some guides like [this](https://proandroiddev.com/mvvm-with-kotlin-android-architecture-components-dagger-2-retrofit-and-rxandroid-1a4ebb38c699),[and this](https://itnext.io/android-architecture-hilt-mvvm-kotlin-coroutines-live-data-room-and-retrofit-ft-8b746cab4a06)
The following schema represents a general idea of the architecture:


![Architecture](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)

Starting from the bottom you have 2 main data types, a CurrentSearchItem and a DetailedItem both are child classes from SearchItem (this is the reason behind we code just one ListAdapter for the whole app without much boilerplate code).
CurrentSearchItem plays when you search for something and the results come up on the result screen.

If you touch on any CurrentSearchItem, the DetailedItem will be used to populate the screen with more information.

We have a local persistent database with room to saves DetailedItems that the user has opened.

Hilt is our man when DependencyInjections come through. It helps us connect the right dependency with the object that needs it. The how is explained [in this nice codelab](https://developer.android.com/codelabs/android-hilt#0) but to keep it short hilt is like a person with a bag of objects, this person gives a specific object to another person who asks for them.  You should be aware of the lifecycle of each component, and ask yourself for example, I need my database for all my app lifecycle?  And you can choose your right scope.

The connector between our data layer and the UI is the ViewModel, for example,  the UI (CurrentSearchFragment) observes searchResult that is a LiveData of type `State<List<CurrentSearchItem>>` from CurrentSearchViewModel, this ViewModel can: 
 - asks to searchAppRepository to perform an online search from a given query.
 - ask for the next batch of information for that query, making this brief who trigger the search is _currentOffSet when the value changes the ViewModel ask for more data with the currentOffset and of course with our original query String. In this way, we just have to increase the currentOffSet number when the next page is required.
A valid question is, why if you're observing a  LiveData of type `State<List<CurrentSearchItem>>` on the UI, the "input"  who triggers the changes inside the ViewModel is an Int? well thanks to switchMap, we can observe the int changes and transform the output to the expected one, because the performOnlineSearch method of our searchAppRepository returns our required type.

Finally, our UI doesn't contain any data logic.

### Architecture - Networking

Retrofit is in charge of the network requests, Moshi is autogenerating the adapters for each model. Abstract class BaseDataSource is making the Error Handling, the main idea is to covert the errors to code errors, to pass it to the UI. On UI we can ask the right string for each error, with that in mind, the multilanguage can be easily implemented.

### Architecture - State


We are using a generic data class called [State](app/src/main/java/com/amaca/searchthapp/datalayer/state/State.kt) all over our data flux, it's very simple, State can have 3 different Status: SUCCESS, ERROR, and LOADING, status function success receive a data parameter T and create a new State with Success Status and data of T but with any error code, error method creates a new State with error Status  with a passed errorCode and without any data. Using the right status allows the app UI to effortlessly manage successful network calls or display errors to the user.


### Architecture - ErrorHandling

Internally, app communicate errors with Integers, the UI uses the error number to map String on string.xml with the right codeError in order to make the translation to other languages easier
# CI.

We will work on CI using GitLab CI/CD. You can see the coverage Visualization on the Merge Request, please see more (how to implement it on GoogleCloud + docker's source code) at [my medium post](https://medium.com/nerd-for-tech/android-continuous-integration-and-coverage-visualization-on-gitlab-4a15d58f624c)

As said on the Medium Post, we will run the CI-pipelines using a physical phone, to get a near-to-real enviroment.

![](readmeResources/ci_on_device.gif)


Coverage percentages near to 100% are certainly reachable but in my experience, you will see few projects getting those numbers, the idea is to try to test the main things and make a good tradeoff between TestingQuality/Time. "The gains of increasing code coverage beyond a certain point are logarithmic" [Google Testing Post](https://testing.googleblog.com/2020/08/code-coverage-best-practices.html) 

Testing classes are under app/src/androidTest/java/com/amaca/searchthapp and the paths of this section will be relative to it.

### CI-UI

The first step to test the UI is to make the thing decoupled as possible from the data, the data is passed from view models and the UI will not contain any kind of logic regard to  data processing but data visualization like the code to change the price from 6000 to $6.000.

The testing is driven to UI behaivor, checking  expected after a given steps, the UI test can be found on /UIBehaivorTest.kt

# Security.

When the app is going prod. we will ensure other developers don't know about the internal of the app, we will obfuscate our code using some free tools anyways in a real-world environment, we should go for the paid tools to get the best-as-possible anti-reverse engineering protection.

`gradle assemble`, `gradle assembleGoogleRelease` or `gradle assembleHuaweiRelease` will build a app with minify so it difficults reverse engineering, there are some other methods of obfuscation that will not be covered in this post.

# Notation.

Our commit notation will follow:

(feature/hotfix)-(arch-sec-ci-ui):
(description)



