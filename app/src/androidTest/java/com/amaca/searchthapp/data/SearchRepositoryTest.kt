package com.amaca.searchthapp.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.liveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amaca.searchthapp.datalayer.local.DetailedItemsDatabaseDao
import com.amaca.searchthapp.datalayer.models.*
import com.amaca.searchthapp.datalayer.remote.ErrorCodes
import com.amaca.searchthapp.datalayer.remote.RemoteDataSource
import com.amaca.searchthapp.datalayer.repository.SearchRepository
import com.amaca.searchthapp.datalayer.state.State
import junit.framework.Assert.*
import kotlinx.coroutines.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class SearchRepositoryTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    lateinit var onSuccessRemoteSource: RemoteDataSource
    lateinit var onErrorRemoteSource: RemoteDataSource
    lateinit var localDataSource: DetailedItemsDatabaseDao
    lateinit var currentSearchItem: CurrentSearchItem
    lateinit var detailedItemResponse: DetailedItemResponse

    @Before
    fun setup() {
        currentSearchItem = CurrentSearchItem(
                System.currentTimeMillis().toString(),
                "Consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control",
                "649990",
                Shipping(true),
                "https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg",
                Installments(12, 123.123))
        val pictures = ArrayList<DetailedPicture>()
        pictures.add(DetailedPicture("url1"))
        pictures.add(DetailedPicture("url2"))
        pictures.add(DetailedPicture("url3"))
        pictures.add(DetailedPicture("url4"))

        detailedItemResponse = DetailedItemResponse(pictures, "99999", "9999", 2, "permalinkURL")
        onSuccessRemoteSource = TestRemoteSource(call = {
            val currentSearchItems = ArrayList<CurrentSearchItem>()


            currentSearchItems.add(currentSearchItem

            )
            State.success(SearchResponseList(paging(10, 10, 10, 10), currentSearchItems))
        },
                detailedItemCall = {


                    State.success(DetailedObjectResponse(detailedItemResponse))
                })


        onErrorRemoteSource = TestRemoteSource(call = {
            State.error(1)
        }, detailedItemCall = {
            State.error(ErrorCodes.GENERIC_ERROR)
        })


        localDataSource = object : DetailedItemsDatabaseDao {
            var detailedItems: List<DetailedItem> = ArrayList<DetailedItem>()
            var detailedItemsHashMap = HashMap<String, DetailedItem>()

            override fun insert(detailedItem: DetailedItem) {
                if (detailedItems is ArrayList)
                    (detailedItems as java.util.ArrayList<DetailedItem>).add(detailedItem)
                detailedItemsHashMap.put(detailedItem.id, detailedItem)
            }

            override fun getAllPreviousSearchItem(): LiveData<List<DetailedItem>> = liveData {
                emit(detailedItems)
            }

            override fun get(id: String): DetailedItem? {
                return detailedItemsHashMap[id]
            }
        }
    }


    @Test
    fun testSearchRepositoryEmitsStatusSuccessWhenSuccessRemoteSource() {
        val mySearchRepo = SearchRepository(onSuccessRemoteSource, localDataSource)
        lateinit var results: LiveData<State<List<CurrentSearchItem>>>

        runBlocking {
            results = mySearchRepo.performOnlineSearch("anything")
        }

        assertEquals(results.getOrAwaitValue(1).status, State.Status.LOADING)
        sleep(100)
        assertEquals(results.getOrAwaitValue(1).status, State.Status.SUCCESS)
    }

    @Test
    fun testSearchRepositoryEmitsStatusErrorWhenErrorOnRemoteSource() {
        val mySearchRepo = SearchRepository(onErrorRemoteSource, localDataSource)
        lateinit var results: LiveData<State<List<CurrentSearchItem>>>

        runBlocking {
            results = mySearchRepo.performOnlineSearch("anything")
        }


        assertEquals(results.getOrAwaitValue(1).status, State.Status.LOADING)
        sleep(100)
        assertEquals(results.getOrAwaitValue(1).status, State.Status.ERROR)

    }

    @Test
    fun testSearchRepositoryEmitsStatusSuccessWhenSuccessSearchingDetailedItemOnRemoteSource() {
        val mySearchRepo = SearchRepository(onSuccessRemoteSource, localDataSource)
        lateinit var results: LiveData<State<DetailedItem>>
        runBlocking {
            results = mySearchRepo.searchForDetailedItem(currentSearchItem)
        }
        assertEquals(results.getOrAwaitValue(100).status, State.Status.LOADING)
        sleep(1000)
        assertEquals(results.getOrAwaitValue(1).status, State.Status.SUCCESS)
        assertEquals(results.getOrAwaitValue(1).data?.id, currentSearchItem.id)
    }

    @Test
    fun testSearchRepositoryEmitsStatusSuccessWhenSuccessSearchingDetailedItemOnLocalSource() {
        val remoteSearchData = detailedItemResponse
        val detailedItem = DetailedItem(currentSearchItem.id, currentSearchItem.title, remoteSearchData.price,
                currentSearchItem.shipping, remoteSearchData.sold_quantity, currentSearchItem.installments, remoteSearchData.original_price,
                System.currentTimeMillis(), remoteSearchData.permalink, currentSearchItem.thumbnail,
                remoteSearchData.pictures.map { it.secure_url }.toList())
        localDataSource.insert(detailedItem)
        val mySearchRepo = SearchRepository(onErrorRemoteSource, localDataSource)
        lateinit var results: LiveData<State<DetailedItem>>
        runBlocking {
            results = mySearchRepo.searchForDetailedItem(currentSearchItem)
        }
        assertEquals(results.getOrAwaitValue(100).status, State.Status.LOADING)
        sleep(1000)
        assertEquals(results.getOrAwaitValue(1).status, State.Status.SUCCESS)
        assertEquals(results.getOrAwaitValue(1).data?.id, currentSearchItem.id)
    }

    @Test
    fun testSearchRepositoryEmitsStatusErrorWhenErrorSearchingDetailedItemOnLocalAndRemoteSource() {
        val mySearchRepo = SearchRepository(onErrorRemoteSource, localDataSource)
        lateinit var results: LiveData<State<DetailedItem>>
        runBlocking {
            results = mySearchRepo.searchForDetailedItem(currentSearchItem)
        }
        assertEquals(results.getOrAwaitValue(100).status, State.Status.LOADING)
        sleep(1000)
        assertEquals(results.getOrAwaitValue(1).status, State.Status.ERROR)
    }





    private class TestRemoteSource(
            val call: () -> State<SearchResponseList>,
            val detailedItemCall: () -> State<DetailedObjectResponse>
    ) : RemoteDataSource {
        override suspend fun performOnlineSearch(query: String, offset: Int, limit: Int, searchBy: String): State<SearchResponseList> = withContext(Dispatchers.IO) {
            //Originally I was making the CountDownLatch thing here...
            call.invoke()
        }

        override suspend fun getDetailedItem(id: String): State<DetailedObjectResponse> = withContext(Dispatchers.IO) {
            detailedItemCall.invoke()
        }
    }


    /***
     * Code taken from: https://medium.com/androiddevelopers/unit-testing-livedata-and-other-common-observability-problems-bb477262eb04
     * it looks pretty neat and a best looking of .observeForever()
     */
    fun <T> LiveData<T>.getOrAwaitValue(
            time: Long = 2,
            timeUnit: TimeUnit = TimeUnit.SECONDS
    ): T {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(o: T?) {
                data = o
                latch.countDown()
                this@getOrAwaitValue.removeObserver(this)
            }
        }

        this.observeForever(observer)

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

        @Suppress("UNCHECKED_CAST")
        return data as T
    }


}