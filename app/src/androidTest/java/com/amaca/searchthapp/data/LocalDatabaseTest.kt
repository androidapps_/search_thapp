package com.amaca.searchthapp.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.amaca.searchthapp.datalayer.local.DetailedItemsDatabase
import com.amaca.searchthapp.datalayer.local.DetailedItemsDatabaseDao
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.models.Installments
import com.amaca.searchthapp.datalayer.models.Shipping
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LocalDatabaseTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var searchAppDatabaseDao: DetailedItemsDatabaseDao
    private lateinit var detailedItem:DetailedItem

    @Before
    fun createDatabase() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val searchAppDatabase = Room.inMemoryDatabaseBuilder(context, DetailedItemsDatabase::class.java).allowMainThreadQueries().build()
        searchAppDatabaseDao = searchAppDatabase.detailedItemsDatabaseDao
        val urls =  ArrayList<String>()
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        detailedItem = DetailedItem(123123123L.toString(), "Consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control onsola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control", "729990",  Shipping( true), 1,
                Installments(12, 123.123), "12",  System.currentTimeMillis(), "https://articulo.mercadolibre.com.co/MCO-498547672-xbox-360-slim-4gb-50-1-control-inalam-cable-hdmi-envio-_JM", urls[0], urls)
    }

    @Test
    fun testDatabaseCanInsertDetailedItem(){
        searchAppDatabaseDao.insert(detailedItem)
        val item = searchAppDatabaseDao.get(detailedItem.id)
        assertEquals(item, detailedItem)

    }

    @Test
    fun testDatabaseCanInsertAndLoadDetailedItem(){
        searchAppDatabaseDao.insert(detailedItem)
        val item = searchAppDatabaseDao.get(detailedItem.id)
        assertEquals(item, detailedItem)

    }

}
