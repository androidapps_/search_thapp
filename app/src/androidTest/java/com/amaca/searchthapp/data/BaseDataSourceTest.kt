package com.amaca.searchthapp.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amaca.searchthapp.datalayer.models.DetailedItemResponse
import com.amaca.searchthapp.datalayer.models.DetailedPicture
import com.amaca.searchthapp.datalayer.models.SearchItem
import com.amaca.searchthapp.datalayer.remote.BaseDataSource
import com.amaca.searchthapp.datalayer.remote.ErrorCodes
import com.amaca.searchthapp.datalayer.state.State
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Response
import java.lang.RuntimeException

@RunWith(AndroidJUnit4::class)
class BaseDataSourceTest {
    val responseBody = ResponseBody.create(
    MediaType.parse("application/json"),
    "{\"key\":[\"somestuff\"]}"
    )

    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenGetsCode400(){
       val searchInstance = SearchDataSourceTest {
           return@SearchDataSourceTest Response.error(
                   400, responseBody)
       }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.BAD_REQUEST)

    }

    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenGetsCode403(){
        val searchInstance = SearchDataSourceTest {
            return@SearchDataSourceTest Response.error(
                    403, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.FORBIDDEN_ERROR)

    }

    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenGetsCode404(){
        val searchInstance = SearchDataSourceTest {
            return@SearchDataSourceTest Response.error(
                    404, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.NOT_FOUND)

    }

    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenGetsCode500(){
        val searchInstance = SearchDataSourceTest {
            return@SearchDataSourceTest Response.error(
                    500, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.INTERNAL_SERVER_ERROR)

    }

    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenGetsCode503(){
        val searchInstance = SearchDataSourceTest {
            return@SearchDataSourceTest Response.error(
                    503, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.SERVICE_UNAVAILABLE)
    }
    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenGetsCode999(){
        val searchInstance = SearchDataSourceTest {
            return@SearchDataSourceTest Response.error(
                    999, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.GENERIC_ERROR)
    }

    @Test
    fun testBaseDataSourceEmitsUnknownErrorCodeWhenGetsAException(){
        val searchInstance = SearchDataSourceTestWithException {
            return@SearchDataSourceTestWithException Response.error(
                    999, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.UNKNOWN_ERROR)
    }
    @Test
    fun testBaseDataSourceEmitsRightErrorCodeWhenNoConnection(){
        val searchInstance = SearchDataSourceNoNetwork {
            return@SearchDataSourceNoNetwork Response.error(
                    999, responseBody
            )
        }
        val responseState : State<SearchItem>
        runBlocking {
            responseState = searchInstance.performSearch()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.NO_NETWORK_ERROR)
    }



    @Test
    fun testBaseDataSourceEmitsSuccessDetailedObjectResponse(){
        val searchDataSource = SearchListDataSourceTest {
            val arrayDetailedItemResponse = ArrayList<DetailedItemResponse>()
            val pictures = ArrayList<DetailedPicture>()
            pictures.add(DetailedPicture("url1"))
            pictures.add(DetailedPicture("url2"))
            pictures.add(DetailedPicture("url3"))
            pictures.add(DetailedPicture("url4"))

            arrayDetailedItemResponse.add(DetailedItemResponse(pictures, "99999", "9999", 2, "permalinkURL"))

            return@SearchListDataSourceTest Response.success(arrayDetailedItemResponse)
        }
        val responseState: State<DetailedItemResponse>
        runBlocking {
            responseState = searchDataSource.getDetailedItemResponse()
        }

        assertEquals(responseState.status, State.Status.SUCCESS)
    }


    @Test
    fun testBaseDataSourceEmitsUnknownErrorCodeWhenGetsAExceptionUsingArrayResponse(){
        val searchInstance = SearchListDataSourceTestException {
            return@SearchListDataSourceTestException Response.error(
                    999, responseBody
            )
        }
        val responseState: State<DetailedItemResponse>
        runBlocking {
            responseState = searchInstance.getDetailedItemResponse()
        }
        assertEquals(responseState.status, State.Status.ERROR)
        assertEquals(responseState.errorCode, ErrorCodes.UNKNOWN_ERROR)

    }




    private class SearchDataSourceTest(private val mockApi: suspend () -> Response<SearchItem>) : BaseDataSource(){
        suspend fun performSearch() = getResult {
            mockApi.invoke()
        }
    }
    private class SearchListDataSourceTest(private val mockApi: suspend () -> Response<List<DetailedItemResponse>>) : BaseDataSource(){
        suspend fun getDetailedItemResponse() = getResultFromArrayResponse {
            mockApi.invoke()
        }
    }
    private class SearchListDataSourceTestException(private val mockApi: suspend () -> Response<List<DetailedItemResponse>>) : BaseDataSource(){
        suspend fun getDetailedItemResponse() = getResultFromArrayResponse {
            throw RuntimeException(":)")
            mockApi.invoke()
        }
    }

    @Suppress("UNREACHABLE_CODE")
    private class SearchDataSourceTestWithException(private val mockApi: suspend () -> Response<SearchItem>) : BaseDataSource(){
        suspend fun performSearch() = getResult {
            throw RuntimeException(":)")
            mockApi.invoke()
        }
    }

    @Suppress("UNREACHABLE_CODE")
    private class SearchDataSourceNoNetwork(private val mockApi: suspend () -> Response<SearchItem>) : BaseDataSource(){
        suspend fun performSearch() = getResult {
            throw RuntimeException("Unable to resolve host")
            mockApi.invoke()
        }
    }


}