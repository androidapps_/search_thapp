package com.amaca.searchthapp

import androidx.appcompat.app.AppCompatActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amaca.searchthapp.utils.ListResultsIdlingResource
import com.amaca.searchthapp.utils.TestUtils
import com.amaca.searchthapp.views.MainActivity
import com.amaca.searchthapp.views.adapters.SearchAdapter
import com.amaca.searchthapp.views.detailed_view.DetailedViewActivity
import junit.framework.Assert
import kotlinx.android.synthetic.main.recent_search_fragment.*
import org.junit.Assume
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress



@RunWith(AndroidJUnit4::class)
class AppBorderCasesTest {
    val carSearchInput = "mazda CX5"
    val houseSearchInput = "casas en bogota"
    private var hasInternetConnection = false
    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(
            MainActivity::class.java)

    @Before
    fun setup(){
        return try {
            val timeoutMs = 1500
            val sock = Socket()
            val sockaddr: SocketAddress = InetSocketAddress("8.8.8.8", 53)
            sock.connect(sockaddr, timeoutMs)
            sock.close()
            hasInternetConnection = true
        } catch (e: IOException) {
            hasInternetConnection =false
        }
    }

    @Test
    fun testAppCanSearchForCars(){
        Assume.assumeTrue(hasInternetConnection)
        lateinit var mainActivity: AppCompatActivity

        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(carSearchInput))
        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val currentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(carSearchInput)

        val idlingResource = ListResultsIdlingResource(currentSearchFragment?.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)


        Thread.sleep(1000)
        IdlingRegistry.getInstance().unregister(idlingResource)
        Assert.assertTrue(currentSearchFragment?.recent_search_items_recyclerview?.adapter?.itemCount!! > 0)
    }

    @Test
    fun testAppCanOpenDetailsWhenSearchHouses(){
        Assume.assumeTrue(hasInternetConnection)
        lateinit var mainActivity: AppCompatActivity

        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(houseSearchInput))
        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val currentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(carSearchInput)

        val idlingResource = ListResultsIdlingResource(currentSearchFragment?.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)


        Thread.sleep(1000)
        IdlingRegistry.getInstance().unregister(idlingResource)

        Intents.init()
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<SearchAdapter.ViewHolder>(1,
                    ViewActions.click()
                ))
        Intents.intended(IntentMatchers.hasComponent(DetailedViewActivity::class.java.name))
        Intents.release()
    }
}