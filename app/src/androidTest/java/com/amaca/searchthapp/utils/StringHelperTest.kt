package com.amaca.searchthapp.utils

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amaca.searchthapp.views.helper.StringHelper
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class StringHelperTest {
    @Test
    fun testSHGetsExpectedPercentageDiscount(){
        val priceDiscount = StringHelper.getPercentageDiscount(134.23, 113.2)
        assertEquals(priceDiscount, "15.66% OFF")
    }

    @Test
    fun testSHGetsEmptyPercentageDiscountIfPriceGreaterThanOriginal(){
        val priceDiscount = StringHelper.getPercentageDiscount( 113.2,134.23)
        assertEquals(priceDiscount, "")
    }

}