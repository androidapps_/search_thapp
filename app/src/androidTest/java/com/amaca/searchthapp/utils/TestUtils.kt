package com.amaca.searchthapp.utils

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import org.hamcrest.CoreMatchers
import org.hamcrest.Description
import org.hamcrest.Matcher

class TestUtils {
    companion object {
        fun typeSearchViewText(text: String?): ViewAction? {
            return object : ViewAction {
                override fun getConstraints(): Matcher<View> {
                    //Ensure that only apply if it is a SearchView and if it is visible.
                    return CoreMatchers.allOf(ViewMatchers.isDisplayed(), ViewMatchers.isAssignableFrom(SearchView::class.java))
                }

                override fun getDescription(): String {
                    return "Change view text"
                }

                override fun perform(uiController: UiController?, view: View) {
                    (view as SearchView).setQuery(text, true)
                }
            }
        }

        fun atPosition(position: Int, itemMatcher: Matcher<View?>): Matcher<View?>? {
            checkNotNull(itemMatcher)
            return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("has item at position $position: ")
                    itemMatcher.describeTo(description)
                }

                override fun matchesSafely(view: RecyclerView): Boolean {
                    val viewHolder = view.findViewHolderForAdapterPosition(position)
                            ?: // has no item on such position
                            return false
                    return itemMatcher.matches(viewHolder.itemView)
                }
            }
        }
    }



}
internal class ListResultsIdlingResource(private val mRecyclerListings: RecyclerView?) :
    IdlingResource {
    private var callback: IdlingResource.ResourceCallback? = null
    override fun isIdleNow(): Boolean {
        if (mRecyclerListings != null && mRecyclerListings.adapter!!.itemCount > 0) {
            if (callback != null) {
                callback!!.onTransitionToIdle()
            }
            return true
        }
        return false
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.callback = callback
    }

    override fun getName(): String {
        return "Recycler idling resource"
    }

}

