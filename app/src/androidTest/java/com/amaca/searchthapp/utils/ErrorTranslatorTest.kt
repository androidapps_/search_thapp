package com.amaca.searchthapp.utils

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.amaca.searchthapp.R
import com.amaca.searchthapp.datalayer.remote.ErrorCodes
import com.amaca.searchthapp.views.MainActivity
import com.amaca.searchthapp.views.helper.ErrorTranslator
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ErrorTranslatorTest {

    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(
            MainActivity::class.java)

    private lateinit var activity: MainActivity

    @Before
    fun setup(){
        activityRule.scenario.onActivity {
            activity = it
        }
    }

    @Test
    fun testRightErrorCodeGivesDetailedError(){
        val error = ErrorTranslator.translateError(ErrorCodes.DETAILED_ID_ERROR, activity)
        assertEquals(error,  activity.applicationContext.getString(R.string.error_detailed_id))
    }

    @Test
    fun testRightErrorCodeGivesUnknowError(){
        val error = ErrorTranslator.translateError(ErrorCodes.UNKNOWN_ERROR, activity)
        assertEquals(error,  activity.applicationContext.getString(R.string.error_unknown_error))
    }


    @Test
    fun testRightErrorCodeGivesNotFoundError(){
        val error = ErrorTranslator.translateError(ErrorCodes.NOT_FOUND, activity)
        assertEquals(error,  activity.applicationContext.getString(R.string.error_not_found))
    }

    @Test
    fun testRightErrorCodeGivesNoNetworkError(){
        val error = ErrorTranslator.translateError(ErrorCodes.NO_NETWORK_ERROR, activity)
        assertEquals(error,  activity.applicationContext.getString(R.string.error_no_network))
    }


    @Test
    fun testNotMappedErrorCodeGivesGenericError(){
        val error = ErrorTranslator.translateError(ErrorCodes.FORBIDDEN_ERROR, activity)
        assertEquals(error,  activity.applicationContext.getString(R.string.error_generic_error))
    }

}