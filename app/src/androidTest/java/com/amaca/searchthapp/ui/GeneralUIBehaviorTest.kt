package com.amaca.searchthapp.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.amaca.searchthapp.utils.ListResultsIdlingResource
import com.amaca.searchthapp.R
import com.amaca.searchthapp.utils.TestUtils
import com.amaca.searchthapp.datalayer.models.CurrentSearchItem
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.models.Installments
import com.amaca.searchthapp.datalayer.models.Shipping
import com.amaca.searchthapp.views.MainActivity
import com.amaca.searchthapp.views.adapters.SearchAdapter
import com.amaca.searchthapp.views.current_search.CurrentSearchFragment
import com.amaca.searchthapp.views.detailed_view.DetailedViewActivity
import com.amaca.searchthapp.views.recent_search.RECENT_SEARCH_FRAGMENT_TAG
import com.amaca.searchthapp.views.recent_search.RecentSearchFragment
import kotlinx.android.synthetic.main.recent_search_fragment.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class GeneralUIBehaviorTest {
    private lateinit var firstSearchInput: String
    private lateinit var secondSearchInput: String
    private lateinit var currentSearchHeaderFirstSearch: String
    private lateinit var currentSearchHeaderSecondSearch: String
    private lateinit var recentSearchHeader: String
    private lateinit var detailedItem: DetailedItem
    private var recentSearchList: ArrayList<DetailedItem> = ArrayList()
    private var currentSearchItems: ArrayList<CurrentSearchItem> = ArrayList()



    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(
        MainActivity::class.java)

    @Before
    fun initValidString() {
        // Specify a valid string.
        firstSearchInput = "Xbox One"
        secondSearchInput = "Starcraft"
        activityRule.scenario.onActivity {
            currentSearchHeaderFirstSearch = it.resources.getString(R.string.current_search_text, firstSearchInput)
            recentSearchHeader = it.resources.getString(R.string.recent_search_text)
            currentSearchHeaderSecondSearch = it.resources.getString(R.string.current_search_text, secondSearchInput)
        }
        val urls =  ArrayList<String>()
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        urls.add("https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg")
        detailedItem = DetailedItem(123123123L.toString(), "Consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control onsola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control", "729990",  Shipping( true), 1,
                Installments(12, 123.123), "12",  System.currentTimeMillis(), "https://articulo.mercadolibre.com.co/MCO-498547672-xbox-360-slim-4gb-50-1-control-inalam-cable-hdmi-envio-_JM", urls[0], urls)

        for (i in 1..15){
            recentSearchList.add(  DetailedItem(123123123L.toString(), i.toString()+"Consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control onsola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control", "729990",  Shipping( i % 2 == 0), i,
                    Installments(12, 123.123), "12",  System.currentTimeMillis(), "https://articulo.mercadolibre.com.co/MCO-498547672-xbox-360-slim-4gb-50-1-control-inalam-cable-hdmi-envio-_JM", urls[0], urls)
            )
        }

        for (i in 0..15) {
            currentSearchItems.add(CurrentSearchItem(System.currentTimeMillis().toString(), i.toString() + "Consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control consola Xbox One S 1tb + Juego Gears Of War 4 +4k+ Control",
                    "649990", Shipping( i % 2 == 0), "https://http2.mlstatic.com/D_711209-MCO44586988009_012021-O.jpg",
                    Installments(12, 123.123)
            ))
        }
    }

    @Test
    fun testSearchChangesTextOfTheHeader() {
        onView(withId(R.id.items_search_view)).perform(click())
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))
        onView(withId(R.id.items_search_view)).perform(click())
        onView(withId(R.id.recent_search_items_recyclerview)).check(matches(TestUtils.atPosition(0, hasDescendant(withText(currentSearchHeaderFirstSearch)))))

    }


    @Test
    fun testRecentSearchAppearsAfterBackButtonFromCurrentSearch(){
        onView(withId(R.id.items_search_view)).perform(click())
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))
        Espresso.pressBack() // To loss focus from the keyboard
        Espresso.pressBack()
        onView(withId(R.id.recent_search_items_recyclerview)).check(matches(TestUtils.atPosition(0, hasDescendant(withText(recentSearchHeader)))))
    }

    @Test
    fun testClickOnItemOfRecentSearchOpenDetailedActivity(){
        Intents.init()
        lateinit var mainActivity: AppCompatActivity
        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val recentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(RECENT_SEARCH_FRAGMENT_TAG)
        (recentSearchFragment as RecentSearchFragment).displayRecentSearchData(recentSearchList)
        val idlingResource = ListResultsIdlingResource(recentSearchFragment.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)

        Thread.sleep(100)


        onView(withId(R.id.recent_search_items_recyclerview))
                .perform(RecyclerViewActions.actionOnItemAtPosition<SearchAdapter.ViewHolder>(1, click()))

        IdlingRegistry.getInstance().unregister(idlingResource)

        Intents.intended(hasComponent(DetailedViewActivity::class.java.name))
        Intents.release()


    }




    @Test
    fun testSearchChangesTextOfTheHeaderAfterMultiSearchUsingInternet(){
        onView(withId(R.id.items_search_view)).perform(click())
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))
        onView(withId(R.id.recent_search_items_recyclerview)).check(matches(TestUtils.atPosition(0, hasDescendant(withText(currentSearchHeaderFirstSearch)))))
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(secondSearchInput))
        onView(withId(R.id.recent_search_items_recyclerview)).check(matches(TestUtils.atPosition(0, hasDescendant(withText(currentSearchHeaderSecondSearch)))))
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))
        onView(withId(R.id.recent_search_items_recyclerview)).check(matches(TestUtils.atPosition(0, hasDescendant(withText(currentSearchHeaderFirstSearch)))))
    }

    @Test
    fun testClickOnItemOfCurrentSearchOpenDetailedActivity(){
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))
        lateinit var mainActivity: AppCompatActivity

        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val currentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(firstSearchInput)
        (currentSearchFragment as CurrentSearchFragment).displayNextBatchOfData( currentSearchItems)
        val idlingResource = ListResultsIdlingResource(currentSearchFragment.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)


        Thread.sleep(1000)




        Intents.init()

        currentSearchFragment.openDetailedView(detailedItem)
        IdlingRegistry.getInstance().unregister(idlingResource)


        Intents.intended(hasComponent(DetailedViewActivity::class.java.name))
        Intents.release()
    }

    @Test
    fun testScrollUpAndDownAndUseSearchBar(){
        lateinit var mainActivity: AppCompatActivity
        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val recentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(RECENT_SEARCH_FRAGMENT_TAG)
        (recentSearchFragment as RecentSearchFragment).displayRecentSearchData(recentSearchList)
        val idlingResource = ListResultsIdlingResource(recentSearchFragment.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)


        Thread.sleep(100)


        onView(withId(R.id.search_card)).perform(ViewActions.swipeDown())
        onView(withId(R.id.search_card)).perform(ViewActions.swipeUp())
        onView(withId(R.id.search_card)).perform(ViewActions.swipeDown())


//        onView(withId(R.id.recent_search_items_recyclerview))
//                .perform(RecyclerViewActions.scrollToPosition<SearchAdapter.ViewHolder>(recentSearchList.size-1))
//
//        onView(withId(R.id.recent_search_items_recyclerview)).perform(RecyclerViewActions.scrollToPosition<SearchAdapter.ViewHolder>(0))

        onView(withId(R.id.items_search_view)).perform(click())
        onView(withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))
        onView(withId(R.id.items_search_view)).perform(click())
        onView(withId(R.id.recent_search_items_recyclerview)).check(matches(TestUtils.atPosition(0, hasDescendant(withText(currentSearchHeaderFirstSearch)))))
    }

    @Test
    fun testOpenRecentSearchItemAndSwipeHorizontalOnGallery(){
        Intents.init()
        lateinit var mainActivity: AppCompatActivity
        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val recentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(RECENT_SEARCH_FRAGMENT_TAG)
        (recentSearchFragment as RecentSearchFragment).displayRecentSearchData(recentSearchList)
        val idlingResource = ListResultsIdlingResource(recentSearchFragment.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)

        Thread.sleep(100)



        onView(withId(R.id.recent_search_items_recyclerview))
                .perform(RecyclerViewActions.actionOnItemAtPosition<SearchAdapter.ViewHolder>(1, click()))

        IdlingRegistry.getInstance().unregister(idlingResource)

        Intents.intended(hasComponent(DetailedViewActivity::class.java.name))
        Intents.release()
        onView(withId(R.id.gallery_recyclerview_detailed_view)).perform(ViewActions.swipeLeft())

        val imageQuantity = detailedItem.imagesURL.size
        onView(withId(R.id.image_number_detailed_view)).check(matches(withText(mainActivity.resources.getString(R.string.image_position_gallery_text,2, imageQuantity))))

        onView(withId(R.id.gallery_recyclerview_detailed_view)).perform(ViewActions.swipeRight())
        onView(withId(R.id.gallery_recyclerview_detailed_view)).perform(ViewActions.swipeRight())
        onView(withId(R.id.image_number_detailed_view)).check(matches(withText(mainActivity.resources.getString(R.string.image_position_gallery_text, 1, imageQuantity))))
    }

}