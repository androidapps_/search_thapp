package com.amaca.searchthapp.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.amaca.searchthapp.R
import com.amaca.searchthapp.utils.ListResultsIdlingResource
import com.amaca.searchthapp.utils.TestUtils
import com.amaca.searchthapp.views.MainActivity
import com.amaca.searchthapp.views.adapters.SearchAdapter
import junit.framework.Assert.assertTrue
import kotlinx.android.synthetic.main.recent_search_fragment.*
import org.junit.Assume.assumeTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress

@RunWith(AndroidJUnit4::class)
@LargeTest
class CurrentSearchFragmentUIBehaviorTest {
    val firstSearchInput = "DeWalt"
    private var hasInternetConnection = false
    @get:Rule
    val activityRule: ActivityScenarioRule<MainActivity> = ActivityScenarioRule(
            MainActivity::class.java)

    @Before
    fun setup(){
        return try {
            val timeoutMs = 1500
            val sock = Socket()
            val sockaddr: SocketAddress = InetSocketAddress("8.8.8.8", 53)
            sock.connect(sockaddr, timeoutMs)
            sock.close()
            hasInternetConnection = true
        } catch (e: IOException) {
            hasInternetConnection =false
        }
    }

    @Test
    fun testCurrentSearchFragmentCanDisplaySortTypes() {
        assumeTrue(hasInternetConnection)
        lateinit var mainActivity: AppCompatActivity

        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))


        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val currentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(firstSearchInput)

        val idlingResource = ListResultsIdlingResource(currentSearchFragment?.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)


        Thread.sleep(1000) //TODO: Remove this and add  execution 'ANDROIDX_TEST_ORCHESTRATOR' when google solve this https://issuetracker.google.com/issues/123987001 probably with AGP7.0.0 }
        IdlingRegistry.getInstance().unregister(idlingResource)
        assertTrue(currentSearchFragment?.recent_search_items_recyclerview?.adapter?.itemCount!! > 0)
        Espresso.onView(ViewMatchers.withId(R.id.order_price_asc)).perform(ViewActions.click())


        Thread.sleep(1000)

        assertTrue(currentSearchFragment.recent_search_items_recyclerview?.adapter?.itemCount!! > 0)
        Espresso.onView(ViewMatchers.withId(R.id.order_price_desc)).perform(ViewActions.click())

        Thread.sleep(1000)
        assertTrue(currentSearchFragment.recent_search_items_recyclerview?.adapter?.itemCount!! > 0)

        Espresso.onView(ViewMatchers.withId(R.id.order_relevance)).perform(ViewActions.click())

        Thread.sleep(1000)
        assertTrue(currentSearchFragment.recent_search_items_recyclerview?.adapter?.itemCount!! > 0)
    }

    @Test
    fun testCurrentSearchFragmentLoadMoreItemsIfScrollBottomRV(){
        assumeTrue(hasInternetConnection)
        lateinit var mainActivity: AppCompatActivity

        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.items_search_view)).perform(TestUtils.typeSearchViewText(firstSearchInput))


        activityRule.scenario.onActivity {
            mainActivity = it
        }
        val currentSearchFragment = mainActivity.supportFragmentManager.findFragmentByTag(firstSearchInput)

        val idlingResource = ListResultsIdlingResource(currentSearchFragment?.recent_search_items_recyclerview)
        IdlingRegistry.getInstance().register(idlingResource)


        Thread.sleep(1000)
        val initialCount = currentSearchFragment?.recent_search_items_recyclerview?.adapter?.itemCount!!
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())
        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.recent_search_items_recyclerview)).perform(ViewActions.swipeUp())

        val finalCount = currentSearchFragment?.recent_search_items_recyclerview?.adapter?.itemCount!!
        assertTrue(finalCount>initialCount)



    }


}