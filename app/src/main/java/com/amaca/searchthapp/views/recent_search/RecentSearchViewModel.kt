package com.amaca.searchthapp.views.recent_search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amaca.searchthapp.datalayer.repository.SearchRepository

class RecentSearchViewModel @ViewModelInject constructor(private val searchAppRepository: SearchRepository): ViewModel() {
    var recentSearchDetailedItems = searchAppRepository.loadRecentSearchDetailedItems()

    var _guideMessageDisplayed = MutableLiveData<Boolean>()

    val guideMessageDisplayed = _guideMessageDisplayed

    init {
        _guideMessageDisplayed.value = false
    }
    fun messageDisplayed (){
        _guideMessageDisplayed.value = true
    }

}