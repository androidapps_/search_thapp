package com.amaca.searchthapp.views.helper

import java.text.NumberFormat
import java.util.*

class StringHelper {
    companion object{
        fun getPercentageDiscount(originalPrice: Double, price: Double): String {
            return if (originalPrice > price)
                ((1 - (price / originalPrice ) )* 100).toString().slice(0..4)+ "% OFF"
            else
                ""
        }

        fun truncateStringAt(string: String, index: Int, appendAtTheEnd: String = "") : String{
            return if(string.length > index) string.substring(0, index) + appendAtTheEnd else string
        }


        fun parseNumberToMoney(number: Double): String {
            val formatter: NumberFormat = NumberFormat.getCurrencyInstance(Locale("es","CO"))
            return formatter.format(number).split(',')[0]


        }
    }
}