package com.amaca.searchthapp.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.amaca.analytics.Analytics
import com.amaca.searchthapp.R
import com.amaca.searchthapp.views.current_search.CURRENT_SEARCH_KEY
import com.amaca.searchthapp.views.current_search.CurrentSearchFragment
import com.amaca.searchthapp.views.helper.SearchBarAnimationHelper
import com.amaca.searchthapp.views.recent_search.RECENT_SEARCH_FRAGMENT_TAG
import com.amaca.searchthapp.views.recent_search.RecentSearchFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Math.abs

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val cardLayoutMargin by lazy { resources.getDimension(R.dimen.cardview_margin_top).toInt() }
    private val cardLayoutMarginHorizontal by lazy { resources.getDimension(R.dimen.text_padding_left) .toInt()}
    private val cardHeight by lazy { resources.getDimension(R.dimen.cardview_height) }
    private val meLiYellow by lazy { ContextCompat.getColor(applicationContext, R.color.meli_yellow) }
    private val whiteColor by lazy { ContextCompat.getColor(applicationContext, R.color.white) }
    private val cardViewRadius by lazy { resources.getDimension(R.dimen.card_view_radius) }

    private lateinit var searchBarCard: CardView

    private var isSearchViewExpanded = false

    private val SCROLL_THRESHOLD = 0f
    private  var verticalStartingPoint = 0f

    private lateinit var fragment: Fragment
    //TODO preserve searchbar state

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_SearchThapp)
        setContentView(R.layout.activity_main)

        if(savedInstanceState == null){
            fragment = RecentSearchFragment()
            supportFragmentManager.beginTransaction().replace(R.id.main_fragment, fragment, RECENT_SEARCH_FRAGMENT_TAG).commit()
        }
        searchBarCard = search_card
        val searchBarTextView = items_search_view
        searchBarTextView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let{
                    val fragmentByTag = supportFragmentManager.findFragmentByTag(query)
                    if(fragmentByTag == null){
                        val bundle = Bundle()
                        bundle.putString(CURRENT_SEARCH_KEY, query)
                        val currentSearchFragment = CurrentSearchFragment()
                        currentSearchFragment.arguments = bundle
                        supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).
                        replace(R.id.main_fragment, currentSearchFragment, query)
                                .addToBackStack(null).commit()
                    }
                    else{
                        supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).
                        replace(R.id.main_fragment, fragmentByTag, query)
                                .addToBackStack(null).commit()
                    }

                }

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                //change
                if (newText?.length == 0){
                    minimizeSearchViewCard()
                }else if(!isSearchViewExpanded){
                    expandSearchViewCard()
                }
                return false
            }


        })

        header_image_view.setOnClickListener { v->
            val recentSearchFragment = supportFragmentManager.findFragmentByTag(RECENT_SEARCH_FRAGMENT_TAG)
            recentSearchFragment?.let {fragment ->
                if(!fragment.isVisible){
                    supportFragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left).
                    replace(R.id.main_fragment, recentSearchFragment)
                            .addToBackStack(null).commit()
                }
            }

        }
        // Toast.makeText(applicationContext, Analytics.startTracking(), Toast.LENGTH_SHORT).show() TODO uncomment this line if you want to see a visual feedback of your current "Analytics" Module
    }



    /***
     * We are going to implement a sort of gesture behavior, during whole app cycle, the scroll down event will trigger
     * expandSearchViewCard and the scroll up will be trigger minimizeSearchViewCard.
     * Why  in dispatchTouchEvent? Well remember the activity (cause is the view at the bottom of the stack)
     * is the last on get the touchEvent/first getting dispatchTouchEvent, we  want to implement this
     * behavior no matter what is (searchviewbar, fragment etc..) above of the activity view..
     */
    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {

            MotionEvent.ACTION_DOWN -> {
                verticalStartingPoint = event.y
            }

            MotionEvent.ACTION_UP -> {

                val verticalDiff = verticalStartingPoint - event.y
                if (abs(verticalDiff) > SCROLL_THRESHOLD){
                    if (verticalDiff>0 && !isSearchViewExpanded)
                        expandSearchViewCard()
                    else if(verticalDiff<0 && isSearchViewExpanded)
                        minimizeSearchViewCard()

                }
            }
        }
        return super.dispatchTouchEvent(event)
        //We don't want to handle touch event but detect the kind of MotionEvent. We will ensure
        //that our ListAdapter manage the click when we set up the click listener...
    }



    private fun minimizeSearchViewCard(){
        SearchBarAnimationHelper.minimizeSearchViewCard(searchBarCard, cardLayoutMargin, cardLayoutMarginHorizontal, whiteColor, cardHeight, cardViewRadius)
        isSearchViewExpanded = false
    }

    private fun expandSearchViewCard(){
        SearchBarAnimationHelper.expandSearchViewCard(searchBarCard, meLiYellow, 0f)
        isSearchViewExpanded = true
    }



}