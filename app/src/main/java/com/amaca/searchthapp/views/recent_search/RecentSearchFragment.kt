package com.amaca.searchthapp.views.recent_search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amaca.searchthapp.R
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.LOCAL_ITEMS_NOT_FOUND
import com.amaca.searchthapp.views.adapters.DetailedItemClickListener
import com.amaca.searchthapp.views.adapters.SearchAdapter
import com.amaca.searchthapp.views.detailed_view.DetailedViewActivity
import com.amaca.searchthapp.views.helper.ErrorTranslator
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.recent_search_fragment.view.*

val DETAILED_ITEM_OBJECT = "detailed_object"
val RECENT_SEARCH_FRAGMENT_TAG = "RECENT_SEARCH_FRAGMENT_TAG"

@AndroidEntryPoint
class RecentSearchFragment : Fragment() {
    private lateinit var listRecentSearch: RecyclerView
    private lateinit var recentSearchAdapter: SearchAdapter
    private val recentSearchViewModel: RecentSearchViewModel by viewModels()
    private var guideMessageDisplayed = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val listRecentSearchFragmentView = inflater.inflate(R.layout.recent_search_fragment, container,false)

        listRecentSearch = listRecentSearchFragmentView.recent_search_items_recyclerview

        listRecentSearch.layoutManager = LinearLayoutManager(listRecentSearch.context)
        listRecentSearch.setHasFixedSize(true)


       val detailedItemClickListener = DetailedItemClickListener{

            val intent = Intent(context, DetailedViewActivity::class.java).apply {
                if (it is DetailedItem)
                    putExtra(DETAILED_ITEM_OBJECT, it)
            }
            startActivity(intent)

        }
        recentSearchAdapter = SearchAdapter(detailedItemClickListener, resources.getString(R.string.recent_search_text))
        listRecentSearch.adapter = recentSearchAdapter


        recentSearchAdapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        recentSearchViewModel.recentSearchDetailedItems.observe(viewLifecycleOwner, Observer { detailedItems ->
            if (detailedItems != null && detailedItems.isNotEmpty())
                displayRecentSearchData(detailedItems)
            else if(!guideMessageDisplayed) {
                displaySnackbarWithError(LOCAL_ITEMS_NOT_FOUND)
                recentSearchViewModel.messageDisplayed()
            }

        })
        recentSearchViewModel.guideMessageDisplayed.observe(viewLifecycleOwner, Observer {
            guideMessageDisplayed = it
        })
        if (savedInstanceState == null) {
            recentSearchAdapter.addHeaderAndSubmitList(ArrayList<DetailedItem>())

        }
        return listRecentSearchFragmentView
    }

    private fun displaySnackbarWithError(errorCode: Int){
        Snackbar.make(requireActivity().findViewById(android.R.id.content),
                ErrorTranslator.translateError(errorCode, requireContext()), Snackbar.LENGTH_LONG)
                .setTextColor(requireContext().getColor(R.color.black))
                .setBackgroundTint(requireContext().getColor(R.color.meli_yellow))
                .show()
    }

    fun displayRecentSearchData(detailedItemList:  List<DetailedItem>){
        recentSearchAdapter.addHeaderAndSubmitList(detailedItemList)

    }




}