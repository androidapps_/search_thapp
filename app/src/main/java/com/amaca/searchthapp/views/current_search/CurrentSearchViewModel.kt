package com.amaca.searchthapp.views.current_search


import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.amaca.searchthapp.datalayer.models.CurrentSearchItem
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.models.paging
import com.amaca.searchthapp.datalayer.remote.MeLiApiSearch
import com.amaca.searchthapp.datalayer.repository.SearchRepository
import com.amaca.searchthapp.datalayer.state.State

class CurrentSearchViewModel @ViewModelInject constructor (
        private val searchAppRepository: SearchRepository)  : ViewModel(){
    private var currentOffset = 0
    companion object{
        const val itemsPerPage = 20
    }
    private val _previousSearchs = ArrayList<CurrentSearchItem>()

    private val _currentOffSet = MutableLiveData<Int>()
    private var currentQuery = ""

    private val _isLoading = MutableLiveData<Boolean>()

    public val isLoading = _isLoading as LiveData<Boolean>
    private val _detailedViewOpened = MutableLiveData<Boolean>()
    val detailedViewOpened = _detailedViewOpened as LiveData<Boolean>

    private var searchBy:String = MeLiApiSearch.RELEVANCE
    init {
        _isLoading.value = false
        _detailedViewOpened.value = true
    }

    private var _searchResult = _currentOffSet.switchMap {
        searchAppRepository.performOnlineSearch(currentQuery, currentOffset , itemsPerPage, searchBy).switchMap { results ->
            liveData {
                results.paging?.let {
                    _isLastPage.value = it.offset >= it.total
                }
                if(results.data != null) {
                    _previousSearchs.addAll(results.data)
                    emit(State(results.status, _previousSearchs as List<CurrentSearchItem>, results.errorCode))
                }else
                    emit(results)
            }

        }
    }

    private val _isLastPage = MutableLiveData<Boolean>()
    val isLastPage = _isLastPage

    private val _currentSearchItem =  MutableLiveData<CurrentSearchItem>()

    var detailedItem = _currentSearchItem.switchMap {
        searchAppRepository.searchForDetailedItem(it)
    }



    val searchResult = _searchResult

    fun search(item: String){
        _isLoading.value = true
        _isLastPage.value = false
        currentQuery = item
        _currentOffSet.value = currentOffset
    }

    fun getNextBatchOfData(){
        _isLoading.value = true
        currentOffset += itemsPerPage
        _currentOffSet.value = currentOffset
    }

    fun dataLoadComplete(){
        _isLoading.value = false
    }

    fun getDetailedItem(currentSearchItem: CurrentSearchItem){
        _detailedViewOpened.value = true
        _currentSearchItem.value = currentSearchItem
    }

    fun detailedItemOpened() {
        _detailedViewOpened.postValue(false)

    }

    fun currentSearchByRelevance() {
        searchBy = MeLiApiSearch.RELEVANCE
        resetSearch()
    }

    fun currentSearchByPriceDesc() {
        searchBy = MeLiApiSearch.PRICE_DESC
        resetSearch()
    }

    fun currentSearchByPriceAsc() {
        searchBy = MeLiApiSearch.PRICE_ASC
        resetSearch()
    }

    private fun resetSearch(){
        _previousSearchs.clear()
        currentOffset = 0
        _currentOffSet.value = 0
    }

}
