package com.amaca.searchthapp.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amaca.searchthapp.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.detailed_view_gallery.view.*

class GalleryDetailItemAdapter : ListAdapter<String, RecyclerView.ViewHolder>(ImageDiffCallback()){



    class ViewHolder private constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        private val imageView: ImageView = itemView.image_gallery_detailed_view
        fun bind(detailedImageURL: String){
            Glide.with(itemView).load(detailedImageURL).fitCenter().into(imageView)
        }

        companion object{
            fun from(parent: ViewGroup): ViewHolder{
                val view = LayoutInflater.from(parent.context).inflate(R.layout.detailed_view_gallery,parent,false)
                return ViewHolder(view)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder){
            is ViewHolder -> {
                holder.bind(getItem(position))
            }
        }

    }


}

class ImageDiffCallback: DiffUtil.ItemCallback<String>(){
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

}