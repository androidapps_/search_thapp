package com.amaca.searchthapp.views.current_search

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amaca.searchthapp.R
import com.amaca.searchthapp.datalayer.models.CurrentSearchItem
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.SEARCH_WITHOUT_RESULTS
import com.amaca.searchthapp.datalayer.state.State
import com.amaca.searchthapp.views.adapters.DetailedItemClickListener
import com.amaca.searchthapp.views.adapters.SearchAdapter
import com.amaca.searchthapp.views.detailed_view.DetailedViewActivity
import com.amaca.searchthapp.views.helper.ErrorTranslator
import com.amaca.searchthapp.views.recent_search.DETAILED_ITEM_OBJECT
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.current_search_fragment.*
import kotlinx.android.synthetic.main.current_search_fragment.view.*
import kotlinx.android.synthetic.main.recent_search_fragment.view.recent_search_items_recyclerview


val CURRENT_SEARCH_KEY = "CURRENT_SEARCH_KEY"

@AndroidEntryPoint
class CurrentSearchFragment : Fragment(){
    private var isDetailedViewItemClosed: Boolean = false
    private lateinit var currentSearchRecyclerView: RecyclerView
    private lateinit var currentSearchAdapter: SearchAdapter
    private var isLoading = false
    private var isLastPage = false
    private var itemsPerPage: Int = 0
    private val currentSearchViewModel: CurrentSearchViewModel by viewModels()
    val currentSearchList: ArrayList<CurrentSearchItem> = ArrayList()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val listRecentSearchFragmentView = inflater.inflate(R.layout.current_search_fragment, container,false)

        currentSearchRecyclerView = listRecentSearchFragmentView.recent_search_items_recyclerview

        val currentSearchLayoutManager = LinearLayoutManager(currentSearchRecyclerView.context)
        currentSearchRecyclerView.layoutManager = currentSearchLayoutManager
        currentSearchRecyclerView.setHasFixedSize(true)


        val detailedItemClickListener = DetailedItemClickListener{
            if(it is CurrentSearchItem)
                currentSearchViewModel.getDetailedItem(it)

        }

        val currentSearch = arguments?.getString(CURRENT_SEARCH_KEY)
                ?: return super.onCreateView(inflater, container, savedInstanceState)

        currentSearchAdapter = SearchAdapter(detailedItemClickListener, resources.getString(R.string.current_search_text, currentSearch), true, removeMarginTopAtHeader = true)
        currentSearchRecyclerView.adapter = currentSearchAdapter





        setupCurrentSearchObserver()

        val dividerItemDecoration = DividerItemDecoration(currentSearchRecyclerView.context,
                currentSearchLayoutManager.orientation)
        currentSearchRecyclerView.addItemDecoration(dividerItemDecoration)
        currentSearchAdapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        currentSearchRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy !=0 && !isLoading && !isLastPage) {
                    val visibleItemCount: Int = currentSearchLayoutManager.childCount
                    val totalItemCount: Int = currentSearchLayoutManager.itemCount
                    val firstVisibleItemPosition: Int = currentSearchLayoutManager.findFirstVisibleItemPosition()
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= itemsPerPage) {
                        currentSearchViewModel.getNextBatchOfData()
                    }
                }
            }

        })
        if (savedInstanceState == null && currentSearchList.size == 0) {
            currentSearchViewModel.search(currentSearch)
            currentSearchAdapter.addHeaderAndSubmitList(currentSearchList)

        }


        setupRedioButtons(listRecentSearchFragmentView)
        return listRecentSearchFragmentView
    }


    private fun setupCurrentSearchObserver() {
        currentSearchViewModel.searchResult.observe(viewLifecycleOwner, Observer {
            indeterminateBar.visibility = View.INVISIBLE
            when(it.status){
                State.Status.SUCCESS -> {
                    it.data?.let { data ->
                        itemsPerPage = data.size
                        if(itemsPerPage>0)
                            displayNextBatchOfData(data)
                        else
                            displaySnackbarWithError(SEARCH_WITHOUT_RESULTS)
                    }
                    currentSearchViewModel.dataLoadComplete()
                }
                State.Status.ERROR -> {
                    it.errorCode?.let {error ->
                        displaySnackbarWithError(error)
                    }
                    currentSearchViewModel.dataLoadComplete()
                }
                State.Status.LOADING -> {
                    indeterminateBar.visibility = View.VISIBLE
                }
            }
        })
        currentSearchViewModel.detailedItem.observe(viewLifecycleOwner, Observer {
            indeterminateBar.visibility = View.INVISIBLE
            when(it.status){
                State.Status.ERROR ->{
                    it.errorCode?.let {error ->
                        displaySnackbarWithError(error)
                    }
                    currentSearchViewModel.detailedItemOpened()
                }
                State.Status.SUCCESS -> {
                    openDetailedView(it.data!!)
                    currentSearchViewModel.detailedItemOpened()
                }
                State.Status.LOADING -> {
                    indeterminateBar.visibility = View.VISIBLE
                }
            }

        })
        currentSearchViewModel.isLastPage.observe(viewLifecycleOwner, Observer {
            isLastPage = it
        })

        currentSearchViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            isLoading = it
        })
        currentSearchViewModel.detailedViewOpened.observe(viewLifecycleOwner, Observer {
            isDetailedViewItemClosed = it
        })
    }

    fun openDetailedView(detailedItem: DetailedItem){
        if(isDetailedViewItemClosed)
            Intent(context, DetailedViewActivity::class.java).apply {
                    putExtra(DETAILED_ITEM_OBJECT, detailedItem)
                    currentSearchViewModel.detailedItemOpened()
                    startActivity(this)
            }
    }
    fun displayNextBatchOfData(dataList: List<CurrentSearchItem>){
        currentSearchAdapter.addHeaderAndSubmitList(dataList)
    }

    private fun setupRedioButtons(view: View){
        view.order_relevance.setOnClickListener {
            orderSearchBy(it)
        }
        view.order_price_desc.setOnClickListener {
            orderSearchBy(it)
        }
        view.order_price_asc.setOnClickListener {
            orderSearchBy(it)
        }
    }

    fun orderSearchBy(view: View) {
        if (view !is RadioButton)
            return
        val checked = view.isChecked
        when (view.id) {
            R.id.order_price_asc -> {
                if (checked)
                    currentSearchViewModel.currentSearchByPriceAsc()
            }
            R.id.order_price_desc -> {
                if (checked)
                    currentSearchViewModel.currentSearchByPriceDesc()
            }
            else -> {
                if (checked)
                    currentSearchViewModel.currentSearchByRelevance()
            }


        }
    }
    private fun displaySnackbarWithError(errorCode: Int){
        Snackbar.make(requireActivity().findViewById(android.R.id.content),
                ErrorTranslator.translateError(errorCode, requireContext()), Snackbar.LENGTH_LONG)
                .setTextColor(requireContext().getColor(R.color.red_error))
                .show()
    }
}