package com.amaca.searchthapp.views.helper

import android.animation.ValueAnimator
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.animation.doOnEnd
import com.amaca.searchthapp.R


class SearchBarAnimationHelper {
    companion object{
        private var animationComplete: Boolean = true


        fun expandSearchViewCard(searchViewCard: CardView, finalColor: Int, radiusWhenAnimationEnds: Float){
            if(!animationComplete)
                return
            animationComplete = false
            val startHeighSize = 100
            val finalHeightSize = 200
            val valueAnimator = ValueAnimator.ofInt(
                startHeighSize,
                finalHeightSize
            )//This is the kind of animation with sizes...
            valueAnimator.duration = 200
            val startTopMargin =
                (searchViewCard.getLayoutParams() as ViewGroup.MarginLayoutParams).topMargin
            //val startMargin = (counter_card.getLayoutParams() as ViewGroup.MarginLayoutParams).leftMargin
            val slopeMargin: Float =
                (-startTopMargin).toFloat() / (finalHeightSize - startHeighSize)
            val biasMargin = -(slopeMargin) * finalHeightSize
            valueAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Int
                searchViewCard.layoutParams.height = value
                val margin = (value * slopeMargin + biasMargin).toInt()
                if (searchViewCard.getLayoutParams() is ViewGroup.MarginLayoutParams) {
                    (searchViewCard.getLayoutParams() as ViewGroup.MarginLayoutParams).setMargins(
                        margin,
                        margin,
                        margin,
                        margin
                    )
                    searchViewCard.requestLayout()
                }
                animation.doOnEnd {
                    animationComplete = true
                }
            }
            searchViewCard.setCardBackgroundColor(finalColor)
            searchViewCard.radius = radiusWhenAnimationEnds
            valueAnimator.start()
        }

        fun minimizeSearchViewCard(searchViewCard: CardView, topMarginWhenAnimationEnds: Int, horizontalMarginWhenAnimationEnds: Int, finalColor:Int, cardHeight: Float, radiusWhenAnimationEnds: Float){
            if(!animationComplete)
                return
            animationComplete = false
            val valueAnimator = ValueAnimator.ofInt(0, 100) // By percentage...
            valueAnimator.duration = 250

            val topMarginWhenAnimationStarts =
                (searchViewCard.getLayoutParams() as ViewGroup.MarginLayoutParams).topMargin

            val biasTop = topMarginWhenAnimationStarts
            val slopeTop =
                (topMarginWhenAnimationEnds - topMarginWhenAnimationStarts) / 100f
            val cardHeightWhenAnimationEnds = cardHeight.toInt()
            val cardHeightWhenAnimationStarts = searchViewCard.layoutParams.height

            val biasCardHeight = cardHeightWhenAnimationStarts
            val slopeCardHeight =
                (cardHeightWhenAnimationEnds - cardHeightWhenAnimationStarts) / 100f
            valueAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Int
                searchViewCard.layoutParams.height =
                    (value * slopeCardHeight + biasCardHeight).toInt()
                if (searchViewCard.getLayoutParams() is ViewGroup.MarginLayoutParams) {
                    (searchViewCard.getLayoutParams() as ViewGroup.MarginLayoutParams).setMargins(
                        horizontalMarginWhenAnimationEnds,
                        (value * slopeTop + biasTop).toInt(),
                        horizontalMarginWhenAnimationEnds,
                        0
                    )
                    searchViewCard.requestLayout()
                }
                animation.doOnEnd {
                    animationComplete = true
                }
            }


            searchViewCard.setCardBackgroundColor(finalColor)

            searchViewCard.radius = radiusWhenAnimationEnds

            valueAnimator.start()

        }
    }
}