package com.amaca.searchthapp.views.helper

import android.content.Context
import com.amaca.searchthapp.R
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.DETAILED_ID_ERROR
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.LOCAL_ITEMS_NOT_FOUND
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.NOT_FOUND
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.NO_NETWORK_ERROR
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.SEARCH_WITHOUT_RESULTS
import com.amaca.searchthapp.datalayer.remote.ErrorCodes.Companion.UNKNOWN_ERROR

class ErrorTranslator {
    companion object {
        fun translateError(int: Int, context: Context): String = when(int){
            DETAILED_ID_ERROR -> context.getString(R.string.error_detailed_id)
            UNKNOWN_ERROR -> context.getString(R.string.error_unknown_error)
            NOT_FOUND -> context.getString(R.string.error_not_found)
            NO_NETWORK_ERROR -> context.getString(R.string.error_no_network)
            LOCAL_ITEMS_NOT_FOUND -> context.getString(R.string.error_local_items_not_found)
            SEARCH_WITHOUT_RESULTS -> context.getString(R.string.error_search_is_empty)
            else -> context.getString(R.string.error_generic_error)
        }


    }
}