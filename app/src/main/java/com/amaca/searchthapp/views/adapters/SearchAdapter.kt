package com.amaca.searchthapp.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amaca.searchthapp.R
import com.amaca.searchthapp.datalayer.models.SearchItem
import com.amaca.searchthapp.views.helper.StringHelper
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.recent_search_item.view.*
import kotlinx.android.synthetic.main.search_header.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/***
 * ListAdapter outperforms Recyclerview in this kind of lists. Android OS can check quickly what exactly changed
 * and use a lot of less resources updating ui.
 */
class SearchAdapter(val clickListener: DetailedItemClickListener, val headerTitle: String, val currentSearchAdapter: Boolean = false, val removeMarginTopAtHeader: Boolean = false) : ListAdapter<DataItem, RecyclerView.ViewHolder>(RecentSearchDiffCallback()){
    private val ITEM_VIEW_TYPE_HEADER = 0
    private val ITEM_VIEW_TYPE_ITEM = 1


    private val adapterScope = CoroutineScope(Dispatchers.Default)

    fun addHeaderAndSubmitList(list: List<SearchItem>?) {
        adapterScope.launch {
            val items = when (list) {
                null -> listOf(DataItem.Header)
                else -> {


                    listOf(DataItem.Header) + list.map { DataItem.SearchDataItem(it) }}
            }
            withContext(Dispatchers.Main) {
                submitList(items)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                val searchItem = getItem(position) as DataItem.SearchDataItem
                holder.itemView.setOnClickListener { clickListener.onClick(searchItem.searchItem) }
                holder.bind(searchItem.searchItem)
            }
        }//        Glide.with(holder.imageSearchItem.context).load("https://i.ytimg.com/vi/kyfqPx4RA5M/maxresdefault.jpg").centerCrop().into(holder.imageSearchItem)
    }
    class ViewHolder private constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        private val imageSearchItem: ImageView = itemView.image_search_item
        private val titleSearchItem: TextView = itemView.description_search_item
        private val priceSearchItem: TextView = itemView.price_search_item
        private val installamentsSearchItem: TextView = itemView.installments_search_item
        val freeShippingSearchItem: TextView = itemView.free_shipping_search_item
        fun bind(detailed: SearchItem){
            var url = if(detailed.thumbnail.contains("https://")) detailed.thumbnail else detailed.thumbnail.replace("http://", "https://")
            url = url.replace("-I.jpg","-O.jpg") // static files with -O.jpg has a higher res than the -I.jpg, source: myself try it hehe
            Glide.with(itemView).load(url).placeholder(R.drawable.header_logo).fitCenter().into(imageSearchItem)
            titleSearchItem.text = StringHelper.truncateStringAt(detailed.title, 80, " ...")
            priceSearchItem.text = StringHelper.parseNumberToMoney(detailed.price.toDouble())
            detailed.installments?.let {
                if (it.quantity != 0)
                    installamentsSearchItem.text = itemView.context.resources.getString(R.string.installaments_text, it.quantity,
                            StringHelper.parseNumberToMoney(it.amount))
                else
                    installamentsSearchItem.text = null
            }
            detailed.shipping?.let {
                if (it.free_shipping)
                    freeShippingSearchItem.text = itemView.context.getText(R.string.free_shipping_text)
                else
                    freeShippingSearchItem.text = null
            }

        }
        companion object{
            fun from(parent: ViewGroup, currentSearchAdapter: Boolean): ViewHolder{
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = if (currentSearchAdapter)  layoutInflater.inflate(R.layout.current_search_item, parent, false) else layoutInflater.inflate(R.layout.recent_search_item, parent, false)
                return ViewHolder(view)
            }
        }

    }

    class HeaderViewHolder(view: View): RecyclerView.ViewHolder(view) {

        companion object {
            fun from(parent: ViewGroup, headerTitle: String, removeMarginTopAtHeader: Boolean = false): HeaderViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.search_header, parent, false)
                if (removeMarginTopAtHeader)view.text_header.setPadding(view.text_header.paddingLeft,0, view.text_header.paddingRight, view.text_header.paddingBottom)
                view.text_header.text = headerTitle
                return HeaderViewHolder(view)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder.from(parent, headerTitle, removeMarginTopAtHeader)
            ITEM_VIEW_TYPE_ITEM -> ViewHolder.from(parent, currentSearchAdapter)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when(getItem(position)){
            is DataItem.SearchDataItem -> ITEM_VIEW_TYPE_ITEM
            is DataItem.Header -> ITEM_VIEW_TYPE_HEADER
            else -> ITEM_VIEW_TYPE_ITEM
        }
    }

}


/***
 * Avoid to re draw all the view when dataset change, it useful when we have a lot of data.
 * Checks the minimum changes to get the newdataset from the olddataset using Myers' difference algorithm
 * see how at: https://blog.robertelder.org/diff-algorithm/
 */
class RecentSearchDiffCallback: DiffUtil.ItemCallback<DataItem>(){
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id && oldItem.title == newItem.title

    }

    /**
     * DataClass like SearchItem autogenerates equal interfaces so, we can compare directly one item with another, but in areItemsTheSame
     * we are going to check only ids for the sake of performance
     */


}

class DetailedItemClickListener(val clickListener: (detailedItem: SearchItem) -> Unit){
    fun onClick(detailedItem: SearchItem) = clickListener(detailedItem)
}

sealed class DataItem {
    data class SearchDataItem(val searchItem: SearchItem): DataItem(){
        override val id =  searchItem.id.replace("MCO","").toLong()
        override val title = searchItem.title
    }

    object Header: DataItem(){
        override val id = Long.MIN_VALUE
        override val title = "HEADER_ANDROID_RECYCLERVIEW_TITLE"
    }

    abstract val id: Long
    abstract val title: String
}