package com.amaca.searchthapp.views.detailed_view

import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.view.View.GONE
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amaca.searchthapp.R
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.views.adapters.GalleryDetailItemAdapter
import com.amaca.searchthapp.views.helper.StringHelper
import com.amaca.searchthapp.views.recent_search.DETAILED_ITEM_OBJECT
import kotlinx.android.synthetic.main.detailed_view_activity.*


class DetailedViewActivity: AppCompatActivity(){
    private lateinit var detailedViewGalleryRecyclerView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.DetailedView)
        setContentView(R.layout.detailed_view_activity)

        val detailedItem: DetailedItem = intent.extras?.get(DETAILED_ITEM_OBJECT) as DetailedItem

        sold_quantity_detailed_view.text = resources.getString(R.string.sold_text, detailedItem.soldItems)
        title_detailed_view.text = StringHelper.truncateStringAt(detailedItem.title, 60, "...")
        val originalPrice = detailedItem.originalPrice?.toDouble()
        val price = detailedItem.price.toDouble()
        if(originalPrice != null && originalPrice>price){

            previous_price_detailed_view.text = StringHelper.parseNumberToMoney(originalPrice)
            previous_price_detailed_view.paintFlags = previous_price_detailed_view.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            discount_detailed_view.text = StringHelper.getPercentageDiscount(originalPrice, price)
        }else{
            previous_price_detailed_view.visibility = GONE
        }

        detailedViewGalleryRecyclerView= gallery_recyclerview_detailed_view
        val detailedViewGalleryAdapter = GalleryDetailItemAdapter()

        image_number_detailed_view.text = resources.getString(R.string.image_position_gallery_text, 1, detailedItem.imagesURL.size)
        detailedViewGalleryRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
            adapter = detailedViewGalleryAdapter
            detailedViewGalleryAdapter.submitList(detailedItem.imagesURL)

            addOnScrollListener(rangeScrollListener(detailedItem.imagesURL.size))

        }




        price_detailed_view.text = StringHelper.parseNumberToMoney(price)
        detailedItem.shipping?.let {
            if (it.free_shipping)
                free_shipping_detailed_view.text = resources.getText(R.string.free_shipping_text)
            else {
                free_shipping_detailed_view.visibility = GONE
            }

        }
        go_to_meli_detailed_view.setOnClickListener {

            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(detailedItem.permalink)
            startActivity(intent)
        }
        supportActionBar?.apply{

            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            title=""
        }


    }

    val imageWidth: Int by lazy{
        detailedViewGalleryRecyclerView.getChildAt(0).measuredWidth
    }

    val halfImageWidth: Int by lazy{ // We don't want to recalculate this every time listener get triggered
        // , so we will use it as a property
        imageWidth / 2
    }



    private val offset = 100


    val upperScrollOffset: Int by lazy {
        imageWidth - offset
    }

    val lowerScrollOffset: Int by lazy {
        offset
    }

    /**
     * This method allow to change the text of current image number based on the RecyclerView position.
     * onScrolled:
     *
     * If is in the first image, it will set the text like 1/10, second image, 2/10 etc....
     *
     * Let's say our width_screen and our imageWidth is 1000
     *
     * Remember that computeHorizontalScrollOffset give us the 'distance' between the starting point  and the current view of the RecyclerView
     *
     * the first element will be between 0 - 1000, the second 1000-2000 and so on.
     *
     * We will listen only on the range of 0-offset and imageWidth - offset. i.e given offset=100:

     * [0, 100] and [900, 1000]
     *
     * We only listen at a short range for the sake of performance.
     *
     * When we say listen, we are referring to performing an action, in this case setting text.
     *
     * onScrollStateChange:
     *
     * When the user stops scrolling when the expected behavior is to go to the closest image.
     *
     */

    private fun rangeScrollListener(arraySize: Int): RecyclerView.OnScrollListener {
        return object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val scrollOffset = recyclerView.computeHorizontalScrollOffset()
                val imageWidthTimes = scrollOffset % imageWidth

                if(imageWidthTimes > upperScrollOffset){
                    val position = (scrollOffset / imageWidth) + 2
                    image_number_detailed_view.text = resources.getString(R.string.image_position_gallery_text, position, arraySize)
                }else if (imageWidthTimes < lowerScrollOffset){
                    val position = (scrollOffset / imageWidth) + 1
                    image_number_detailed_view.text = resources.getString(R.string.image_position_gallery_text, position, arraySize)

                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                val scrollOffset = recyclerView.computeHorizontalScrollOffset()
                super.onScrollStateChanged(recyclerView, newState)
                val imageWidthTimes = scrollOffset % imageWidth
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    var position: Int = (scrollOffset / imageWidth)
                    if(imageWidthTimes > halfImageWidth) {
                        position += 1
                    }
                    recyclerView.smoothScrollToPosition(position)

                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }





}