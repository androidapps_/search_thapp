package com.amaca.searchthapp.datalayer.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "previous_search_table")
data class DetailedItem(
        @PrimaryKey(autoGenerate = false)
        override val id: String,
        @ColumnInfo(name = "title")
        override var title: String,
        @ColumnInfo(name = "price")
        override var price: String,
        @ColumnInfo(name="free_shipping")
        override var shipping: Shipping?,
        @ColumnInfo(name = "sold_items")
        var soldItems: Int,
        @ColumnInfo(name = "installaments_amount")
        override var installments: Installments?,
        @ColumnInfo(name = "original_price")
        var originalPrice: String?,
        @ColumnInfo(name="search_date")
        var searchDate: Long,
        @ColumnInfo(name="link_meli")
        var permalink: String,
        @ColumnInfo(name = "imagesURL")
        override var thumbnail: String,
        @ColumnInfo(name = "images")
        var imagesURL: List<String>
) : Parcelable, SearchItem()


class DetailedItemConverter{
        @TypeConverter
        fun convertBooleanfromShipping(value :Shipping?): Boolean{
                value?.let {

                        return it.free_shipping
                }
                return false
        }

        @TypeConverter
        fun convertShippingFromBoolean(value:Boolean?): Shipping?{
                value?.let {
                        return Shipping(it)
                }
                return null
        }

        @TypeConverter
        fun convertInstallment(value: Installments?): String{
                value?.let {
                        return it.quantity.toString() + "," + it.amount.toString()
                }
                return "none"
        }

        @TypeConverter
        fun convertInstallmentFromString(value: String) : Installments?{
                if (value.equals("none"))
                        return null
                val splittedString = value.split(",")
                if (splittedString.size == 2 && splittedString[0].toIntOrNull() != null &&  splittedString[1].toDoubleOrNull() != null)
                        return Installments(splittedString[0].toInt(), splittedString[1].toDouble())
                else
                        return Installments(0,0.0)
        }


        @TypeConverter
        fun listToJson(value: List<String>?) = Gson().toJson(value)

        @TypeConverter
        fun jsonToList(value: String) = Gson().fromJson(value, Array<String>::class.java).toList()
}