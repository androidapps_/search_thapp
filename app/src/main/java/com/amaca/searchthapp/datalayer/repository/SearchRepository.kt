package com.amaca.searchthapp.datalayer.repository

import androidx.lifecycle.liveData
import com.amaca.searchthapp.datalayer.local.DetailedItemsDatabaseDao
import com.amaca.searchthapp.datalayer.models.CurrentSearchItem
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.remote.MeLiApiSearch
import com.amaca.searchthapp.datalayer.remote.ErrorCodes
import com.amaca.searchthapp.datalayer.remote.RemoteDataSource
import com.amaca.searchthapp.datalayer.state.State
import kotlinx.coroutines.*
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: DetailedItemsDatabaseDao
){
    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)
    fun performOnlineSearch(query: String, offset:Int =  50, limit:Int = 50, searchBy: String = MeLiApiSearch.RELEVANCE) =
            liveData(Dispatchers.IO){
                emit(State.loading())

                val request = remoteDataSource.performOnlineSearch(query, offset, limit, searchBy)
                when(request.status){
                    State.Status.SUCCESS -> {
                        request.data?.let {
                            emit(State.success(it.results, it.paging))
                        }

                    }
                    State.Status.ERROR -> {
                        if (request.errorCode != null)
                            emit(State.error(request.errorCode))
                    }
                    State.Status.LOADING -> {
                        emit(State.loading())
                    }
                }
            }
    fun searchForDetailedItem(currentSearchItem: CurrentSearchItem) = liveData(Dispatchers.IO) {
        emit(State.loading())
        val localSearch = localDataSource.get(currentSearchItem.id)
        if (localSearch == null){
            val remoteSearch = remoteDataSource.getDetailedItem(currentSearchItem.id)
            if(remoteSearch.status == State.Status.SUCCESS && remoteSearch.data != null){
                val remoteSearchData = remoteSearch.data.body
                val detailedItem = DetailedItem(currentSearchItem.id, currentSearchItem.title, remoteSearchData.price,
                currentSearchItem.shipping, remoteSearchData.sold_quantity, currentSearchItem.installments, remoteSearchData.original_price,
                System.currentTimeMillis(), remoteSearchData.permalink, currentSearchItem.thumbnail,
                    remoteSearchData.pictures.map { it.secure_url }.toList())
                saveDetailedItem(detailedItem)
                emit(State.success(detailedItem))


            }else if (remoteSearch.status == State.Status.ERROR && remoteSearch.errorCode != null){
                emit(State.error(remoteSearch.errorCode))

            }else {
                emit(State.error(ErrorCodes.DETAILED_ID_ERROR))
            }
        }else {
            emit(State.success(localSearch))
        }
    }
    private fun saveDetailedItem(detailedItem: DetailedItem) = scope.launch {
        withContext(Dispatchers.IO) {
            localDataSource.insert(detailedItem)
        }
    }

    fun loadRecentSearchDetailedItems() =  liveData(Dispatchers.IO) {
        val localSearch = localDataSource.getAllPreviousSearchItem()
        emitSource(localSearch)
    }


}