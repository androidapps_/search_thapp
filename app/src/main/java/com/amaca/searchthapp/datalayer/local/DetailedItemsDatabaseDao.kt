package com.amaca.searchthapp.datalayer.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.amaca.searchthapp.datalayer.models.DetailedItem

@Dao
interface DetailedItemsDatabaseDao {
    @Insert
    fun insert(detailedItem: DetailedItem)

    @Query("SELECT * FROM previous_search_table ORDER BY search_date DESC")
    fun getAllPreviousSearchItem(): LiveData<List<DetailedItem>>

    @Query("SELECT * FROM previous_search_table where id =:id")
    fun get(id:String): DetailedItem?
}