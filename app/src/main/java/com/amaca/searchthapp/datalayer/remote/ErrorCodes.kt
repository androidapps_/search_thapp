package com.amaca.searchthapp.datalayer.remote

class ErrorCodes {
    //Why not simply? pass message? Because the user must not see any kind of sensible data of the server
    // furthermore, the ui can map errorcode to the correct language
    companion object {

        const val UNKNOWN_ERROR = 0
        const val FORBIDDEN_ERROR: Int =2
        const val GENERIC_ERROR: Int = 3
        const val SERVICE_UNAVAILABLE: Int = 4
        const val INTERNAL_SERVER_ERROR: Int = 5
        const val NOT_FOUND: Int = 6
        const val BAD_REQUEST: Int = 7
        const val NO_NETWORK_ERROR: Int = 8
        const val DETAILED_ID_ERROR: Int = 9
        const val LOCAL_ITEMS_NOT_FOUND: Int = 10
        const val SEARCH_WITHOUT_RESULTS: Int = 11
    }
}

class MeLiApiSearch{
    companion object{
        const val RELEVANCE = "relevance"
        const val PRICE_ASC = "price_asc"
        const val PRICE_DESC = "price_desc"
    }
}

class ServerURL {
    companion object{
        const val PUBLIC_URL = "https://api.mercadolibre.com/"
    }
}
