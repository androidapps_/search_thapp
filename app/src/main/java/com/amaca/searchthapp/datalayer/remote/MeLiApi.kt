package com.amaca.searchthapp.datalayer.remote

import com.amaca.searchthapp.datalayer.models.DetailedObjectResponse
import com.amaca.searchthapp.datalayer.models.SearchResponseList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MeLiApi {
    @GET("sites/MCO/search")
    suspend fun makeSearch(@Query("q") query:String, @Query("offset")offset: Int = 50, @Query("limit") limit:Int = 50,@Query("sort") sort: String = MeLiApiSearch.RELEVANCE) : Response<SearchResponseList>

    @GET("items")
    suspend fun getItemWithID(@Query("ids") id: String) : Response<List<DetailedObjectResponse>>

}