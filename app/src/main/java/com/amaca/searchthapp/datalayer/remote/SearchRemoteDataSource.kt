package com.amaca.searchthapp.datalayer.remote

import com.amaca.searchthapp.datalayer.models.DetailedObjectResponse
import com.amaca.searchthapp.datalayer.models.SearchResponseList
import com.amaca.searchthapp.datalayer.state.State
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchRemoteDataSource @Inject constructor(private val meLiApi: MeLiApi) : BaseDataSource(), RemoteDataSource{
    override suspend fun performOnlineSearch(query: String, offset: Int, limit: Int, searchBy: String): State<SearchResponseList> = getResult {
        meLiApi.makeSearch(query, offset, limit, searchBy)
    }

    override suspend fun getDetailedItem(id: String): State<DetailedObjectResponse> = getResultFromArrayResponse{
        meLiApi.getItemWithID(id)
    }
}

interface RemoteDataSource {
    suspend fun performOnlineSearch(query: String, offset:Int = 50, limit: Int = 50, searchBy: String = MeLiApiSearch.RELEVANCE): State<SearchResponseList>
    suspend fun getDetailedItem(id: String): State<DetailedObjectResponse>
}