package com.amaca.searchthapp.datalayer.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class CurrentSearchItem(override val id: String,
                             override var title: String,
                             override var price: String,
                             override var shipping: Shipping?,
                             override var thumbnail: String,
                             override var installments: Installments?) : Parcelable, SearchItem()