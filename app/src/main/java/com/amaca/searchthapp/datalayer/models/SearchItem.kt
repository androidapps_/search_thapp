package com.amaca.searchthapp.datalayer.models

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize


abstract class SearchItem {
    abstract val id: String
    abstract var title: String
    abstract var price: String
    abstract var shipping: Shipping?
    abstract var thumbnail: String
    abstract var installments: Installments?
}

@JsonClass(generateAdapter = true)
@Parcelize
data class Shipping (
    val free_shipping: Boolean
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class Installments (
        val quantity: Int,
        val amount: Double
) : Parcelable