package com.amaca.searchthapp.datalayer.remote

import com.amaca.searchthapp.datalayer.state.State
import retrofit2.Response

abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): State<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return State.success(body)
            }


            return error(response.code())
        } catch (e: Exception) {
            e.message?.let {
                if (it.contains("Unable to resolve host"))
                    return error(1)
            }
            return error(0)
        }
    }
    protected suspend fun <T> getResultFromArrayResponse(call: suspend () -> Response<List<T>>): State<T> {
        try {

            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return State.success(body[0])
            }
            return error(response.code())
        } catch (e: Exception) {
            e.message?.let {
                if (it.contains("Unable to resolve host"))
                    return error(1)
            }
            return error(0)
        }
    }

    private fun <T> error(code:Int?): State<T> {
        return when(code){
            0 -> State.error(ErrorCodes.UNKNOWN_ERROR)
            1 -> State.error(ErrorCodes.NO_NETWORK_ERROR)
            400 -> State.error(ErrorCodes.BAD_REQUEST)
            403 -> State.error(ErrorCodes.FORBIDDEN_ERROR)
            404 -> State.error(ErrorCodes.NOT_FOUND)
            500 -> State.error(ErrorCodes.INTERNAL_SERVER_ERROR)
            503 -> State.error(ErrorCodes.SERVICE_UNAVAILABLE)
            else -> State.error(ErrorCodes.GENERIC_ERROR)

        }
    }


}