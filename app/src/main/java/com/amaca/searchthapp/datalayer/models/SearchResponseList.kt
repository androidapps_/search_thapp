package com.amaca.searchthapp.datalayer.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SearchResponseList(
    val paging: paging,
    val results: List<CurrentSearchItem>
)

@JsonClass(generateAdapter = true)
data class DetailedItemResponse(
        val pictures: List<DetailedPicture>,
        val price: String,
        val original_price: String?,
        val sold_quantity: Int,
        val permalink: String

)
@JsonClass(generateAdapter = true)
data class DetailedObjectResponse(
    val body: DetailedItemResponse
)

@JsonClass(generateAdapter = true)
data class DetailedPicture(
    val secure_url: String
)

@JsonClass(generateAdapter = true)
data class paging(
    val total: Int,
    val primary_results: Int,
    val offset: Int,
    val limit: Int
)