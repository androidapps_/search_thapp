package com.amaca.searchthapp.datalayer.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.amaca.searchthapp.datalayer.models.DetailedItem
import com.amaca.searchthapp.datalayer.models.DetailedItemConverter

@Database(entities = [DetailedItem::class], version = 1, exportSchema = false)
@TypeConverters(DetailedItemConverter::class)
abstract class DetailedItemsDatabase : RoomDatabase(){
    abstract val detailedItemsDatabaseDao: DetailedItemsDatabaseDao

    companion object{
        @Volatile
        private var INSTANCE: DetailedItemsDatabase? = null

        fun getInstance(context: Context): DetailedItemsDatabase  {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null){
                    instance =
                        Room.databaseBuilder(context.applicationContext,
                            DetailedItemsDatabase::class.java, "previous_search_database").build()
                }
                return instance
            }
        }
    }

}