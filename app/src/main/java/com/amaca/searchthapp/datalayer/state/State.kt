package com.amaca.searchthapp.datalayer.state

import com.amaca.searchthapp.datalayer.models.paging


data class State<out T>(val status: Status, val data: T?, val errorCode: Int?, val paging: paging? = null) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {
        fun <T> success(data: T, paging: paging? = null): State<T> {
            return if (paging != null)
                State(Status.SUCCESS, data, null, paging)
            else
                State(Status.SUCCESS, data, null)
        }

        fun <T> error(errorCode: Int): State<T> {
            return State(Status.ERROR, null, errorCode)
        }

        fun <T> loading(data: T? = null): State<T> {
            return State(Status.LOADING, data, null)
        }
    }
}