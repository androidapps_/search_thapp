package com.amaca.searchthapp.di

import android.content.Context
import com.amaca.searchthapp.datalayer.local.DetailedItemsDatabase
import com.amaca.searchthapp.datalayer.local.DetailedItemsDatabaseDao
import com.amaca.searchthapp.datalayer.remote.MeLiApi
import com.amaca.searchthapp.datalayer.remote.RemoteDataSource
import com.amaca.searchthapp.datalayer.remote.ServerURL
import com.amaca.searchthapp.datalayer.repository.SearchRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
public class AppModule {

    @Provides
    fun providesPins() = CertificatePinner.Builder()
                .add("api.mercadolibre.com", "sha256/NC1YcxYsh996H2Iy7vMtOrd/DpzFothWHAAts3Up+KQ=", "sha256/RQeZkB42znUfsDIIFWIRiYEcKl7nHwNFwWCrnMMJbVc=")
                .build()


    @Singleton
    @Provides
    fun providesRetrofit(certificatePinner: CertificatePinner) : Retrofit =
            Retrofit.Builder().baseUrl(ServerURL.PUBLIC_URL)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .client(OkHttpClient.Builder().
                            certificatePinner(certificatePinner)
                            .addInterceptor { chain -> // Retrofit escapes characters.
                        val request = chain.request()
                        val response = chain.proceed(request)
                        response
                    }.build())
                    .build()



    @Singleton
    @Provides
    fun providesMeLiApi(retrofit: Retrofit): MeLiApi = retrofit.create(MeLiApi::class.java)

    @Singleton
    @Provides
    fun provideRepository(remoteDataSource: RemoteDataSource, localDataSource: DetailedItemsDatabaseDao):SearchRepository = SearchRepository(remoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun providesLocalDatabase(@ApplicationContext appContext: Context) = DetailedItemsDatabase.getInstance(appContext)

    @Singleton
    @Provides
    fun providesDatabaseDao(detailedItemsDatabase: DetailedItemsDatabase) = detailedItemsDatabase.detailedItemsDatabaseDao
}
