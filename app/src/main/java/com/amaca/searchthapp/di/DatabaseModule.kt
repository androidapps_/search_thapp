package com.amaca.searchthapp.di

import com.amaca.searchthapp.datalayer.remote.RemoteDataSource
import com.amaca.searchthapp.datalayer.remote.SearchRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
abstract class DatabaseModule {
    @Singleton
    @Binds
    abstract fun bindSearchRemoteDataSource(impl: SearchRemoteDataSource): RemoteDataSource

}